# Clean Insights SDK

## 2.4.4

- Added `Configuration#check` to check for most common configuration mistakes.

## 2.4.3

- Added `testServer` helper method to `CleanInsights` object, to test for server issues without 
  needing to go through  a full measurement cycle.

## 2.4.2

- Fixed logic of `measureVisit` and `measureEvent` to make sure, `persistAndSend` gets called.
- Updated to latest Kotlin version.

## 2.4.1

- Fixed serious bug when trying to serialize an Insights object. Insights could never be offloaded 
  until now.
- Improved coroutine usage.
- Updated dependencies. 

## 2.4.0

- Added truncated exponential backoff retries on server failures.
- Added automatic purge of too old unsent data.

## 2.3.0

- Renamed `CleanInsights.featureSize` to `featureConsentsSize` and `CleanInsights.campaignSize` to 
  `campaignConsentsSize` for clarification.
- Added some unit tests.

## 2.2.5

- Improved Java interoperability.
- Let DefaultStore override by users. 

## 2.2.4

- Initial working release. 
