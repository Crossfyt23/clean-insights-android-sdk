Clean Insights Android SDK
==========================

Clean Insights gives developers a way to plug into a secure, private measurement platform. 
It is focused on assisting in answering key questions about app usage patterns, and not on 
enabling invasive surveillance of all user habits. 
Our approach provides programmatic levers to pull to cater to specific use cases and privacy needs. 
It also provides methods for user interactions that are ultimately empowering instead of alienating.


## Example

```kotlin

    // Instantiate with configuration and directory to write store to, best in an `Application` subclass.
    val ci = CleanInsights(
        context.assets.open("cleaninsights.json").reader().readText(),
        context.filesDir)

    // Ask for consent:
    ci.requestConsent("test", ConsentRequestUi(this))

    // Measure a page visit, e.g. in `Activity#onResume`:
    ci.measureVisit(listOf("Main"), "test")

    // Measure an event (e.g. a button press):
    ci.measureEvent("music", "play", "test")

    // Make sure to persist the locally cached data. E.g. on `Application#onTrimMemory`, `#onLowMemory` 
    // and `#onTerminate`.
    ci.persist()
```

*Please note*: `CleanInsights`' core concept is a **Campaign**. Since we don't want you to
record just *anything*, like all the others do, you need to configure, what your actually interested
in and for how long. For a deeper understanding, please read the [Concepts](#concepts) section below.

This project also contains an [example app](https://gitlab.com/cleaninsights/clean-insights-android-sdk/-/tree/master/app). 
Please refer to that for a full understanding on how to use this library.


## Installation

CleanInsightsSDK is available through [Maven](https://mvnrepository.com). To install it,
simply add the following line to your `build.gradle` file:

```groovy
implementation 'org.cleaninsights.sdk:clean-insights-sdk:[Latest Version]'
```

In theory, CleanInsightsSDK is also available through [Jitpack](https://jitpack.io/).
However, since Gradle 7, JitPack has issues and we couldn't make the latest versions 
compile there.

To install via JitPack, you would add the following line to your `build.gradle` file:

```groovy
implementation 'com.gitlab.cleaninsights:clean-insights-android-sdk:[Release-Tag]'
```


## Further Documentation:

- [Main project page](http://cleaninsights.org/)

- [All source code](https://gitlab.com/cleaninsights)

- [Design documents and JSON schema specifications](https://gitlab.com/cleaninsights/clean-insights-design)

- [API documentation](https://cleaninsights.gitlab.io/clean-insights-android-sdk/)

- [A Guide to Clean Consent UX](https://okthanks.com/blog/2021/5/14/clean-consent-ux)


## Concepts

### Measurements

A measurement is a single call to the SDK to measure a visit or an event.

Measurements are always aggregated immediately and only stored in that aggregated
form, in order to avoid a too high resolution and therefore unnecessary invasion into the users
privacy.

All measurements are done for a specific campaign you need to configure.

Measurements done against an unconfigured campaign are ignored, as well as measurements
done against a campaign which ran out or where the maximum length of days of data gathering
is crossed.

This helps you avoid unwanted measurements with left-over measurement code of older 
campaigns.


### Campaigns

A campaign has a period during which it is active and a maximum length of days during which 
data is gathered for a specific user after they consented to the measurements.

The specific start date of a campaign helps you coordinate campaigns across platforms.
Measurements done before a campaign start are, of course, ignored.

The days during which measurements take place are defined by the aggregation period length
in days and the number of periods.

After a user consented, measurements will start at the beginning of the next aggregation period
for as long as that period takes.

At the end of a aggregation period, the campaign data will be automatically sent to your insights 
server.

If you configure a higher `numberOfPeriods` than 1, the next aggregation period will begin
immediately after the end of the first one and measurement will continue.


#### Events

In Contrast to visit measurements, event measurements support the complete 
[Matomo Event API](https://matomo.org/docs/event-tracking/).

This means you can also record numeric values, like e.g. time something takes.

You can configure the aggregation method to use, when the same event is recorded multiple 
times: Event values can be summed up or an average can be calculated.

This can be configured per campaign.


#### Not a Matomo Campaign

Note the difference to Matomo: CleanInsights' concept of a campaign doesn't map to Matomo's
concept of campaigns, which is mostly about finding out if a marketing campaign (like an advertisement
or a landing page) sent any additional visitors to a website.


### Consent

You need to ask your users for consent to your measurements.

There are 2 types of consent:

- Campaign consents: Each measurement campaign needs to get consent by the user. You need
to explain to the user what you want to measure, how long you want to measure it and why.

The SDK contains UI to help you with that.

- Common feature consent: Some data is orthogonal to the visits or events you want to measure,
like locale or device type used. Since these features are only ever recorded when doing
measurements for a campaign, consent needs to be gathered only once per user per feature.

A user might want to actively withdraw consent. The SDK supports that. Please make sure that
the user can actually do that.

The SDK provides UI to help you with that.


## Configuration

To make sure your CleanInsights configuration is valid, you can use our 
[JSON scheme to validate against.](https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schemas/configuration.schema.json)

Here's an [online validator](https://www.jsonschemavalidator.net)

A [complete documentation](https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schema-docs/README.md)
is generated from that JSON scheme. 


## Supported Backends

CleanInsights SDK currently supports Matomo as a backend via 
[CIMP](https://gitlab.com/cleaninsights/clean-insights-matomo-proxy).


## Dependency Verification

This library supports dependency verification.
If you run into artifacts failing to verify, you might want to run this command in the project root
to recalculate hashes:

```bash
./gradlew --write-verification-metadata sha256 build pmd
```

Be aware, that you should investigate all changes and make sure, that the updated dependencies don't
contain any unwanted changes!


## Author

Benjamin Erhart, berhart@netzarchitekten.com for the [Guardian Project](https://guardianproject.info)


## License

CleanInsightsSDK is available under the MIT license. See the LICENSE file for more info.

