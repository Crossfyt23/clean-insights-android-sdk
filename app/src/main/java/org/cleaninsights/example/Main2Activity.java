package org.cleaninsights.example;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.cleaninsights.example.databinding.ActivityMainBinding;
import org.cleaninsights.sdk.Feature;

import java.util.Collections;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {

    private final long start = System.currentTimeMillis();

    private boolean firstTime = true;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());

        binding.btConsents.setOnClickListener(this);

        setContentView(binding.getRoot());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (firstTime) {
            JavaConsentRequestUi ui = new JavaConsentRequestUi(this);

            ExampleApp.getCleanInsights().requestConsent("test", ui, granted -> {
                if (!granted) return;

                ExampleApp.getCleanInsights().requestConsent(Feature.Lang, ui,
                        granted1 -> ExampleApp.getCleanInsights().requestConsent(Feature.Ua, ui));

                double time = (System.currentTimeMillis() - start) / 1000.0;

                ExampleApp.getCleanInsights().measureEvent("app-state", "startup-success", "test", "time-needed", time);
                ExampleApp.getCleanInsights().measureVisit(Collections.singletonList("Main"), "test");
            });

            firstTime = false;
        }
        else {
            ExampleApp.getCleanInsights().measureVisit(Collections.singletonList("Main"), "test");
        }
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, ConsentsActivity.class));
    }
}
