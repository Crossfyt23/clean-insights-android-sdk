package org.cleaninsights.sdk

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.lang.RuntimeException
import java.net.URL
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.jvm.isAccessible

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner::class)
class CleanInsightsTests {

    companion object {
        private val conf = Configuration(
                server = URL("http://localhost:8080/ci/cleaninsights.php"),
                siteId = 1,
                campaigns = mapOf(Pair("test", Campaign(
                        start = Instant.parse("2021-01-01T00:00:00Z"),
                        end = Instant.parse("2099-12-31T23:59:59Z"),
                        aggregationPeriodLength = 1,
                        numberOfPeriods = 90,
                        onlyRecordOnce = false,
                        eventAggregationRule = EventAggregationRule.Avg))),
                timeout = 1.0,
                maxRetryDelay = 1.0,
                maxAgeOfOldData = 1,
                persistEveryNTimes = 10,
                serverSideAnonymousUsage = false,
                debug = true)

        private const val confString = "{\n" +
                "    \"server\": \"http://localhost:8080/ci/cleaninsights.php\",\n" +
                "    \"siteId\": 1,\n" +
                "    \"timeout\": 1,\n" +
                "    \"maxRetryDelay\": 1,\n" +
                "    \"maxAgeOfOldData\": 1,\n" +
                "    \"debug\": true,\n" +
                "    \"campaigns\": {\n" +
                "        \"test\": {\n" +
                "            \"start\": \"2021-01-01T00:00:00Z\",\n" +
                "            \"end\": \"2099-12-31T23:59:59Z\",\n" +
                "            \"aggregationPeriodLength\": 1,\n" +
                "            \"numberOfPeriods\": 90,\n" +
                "            \"onlyRecordOnce\": false,\n" +
                "            \"eventAggregationRule\": \"avg\"\n" +
                "        }\n" +
                "    }\n" +
                "}\n"
    }


    private lateinit var ci: CleanInsights

    @get:Rule
    val tempDir = TemporaryFolder()

    @Before
    fun setup() {
        if (storeFile.exists()) {
            assertTrue(storeFile.delete())
        }

        ci = CleanInsights(confString, tempDir.root)
    }


    @Test
    fun conf_isCorrectlyParsed() {
        assertEquals(conf, ci.conf)
    }

    @Test
    fun denyConsent() {
        ci.deny(Feature.Lang)
        ci.deny(Feature.Ua)
        ci.deny("test")

        assertEquals(1, ci.campaignConsentSize)
        assertEquals(2, ci.featureConsentSize)

        var consent: Consent? = ci.getFeatureConsentByIndex(0)
        assertNotNull(consent)
        assertFalse(consent!!.granted)

        consent = ci.getFeatureConsentByIndex(1)
        assertNotNull(consent)
        assertFalse(consent!!.granted)

        consent = ci.getFeatureConsentByIndex(2)
        assertNull(consent)

        consent = ci.getCampaignConsentByIndex(0)
        assertNotNull(consent)
        assertFalse(consent!!.granted)

        consent = ci.getCampaignConsentByIndex(1)
        assertNull(consent)

        assertFalse(ci.isCampaignCurrentlyGranted("test"))
    }

    @Test
    fun grantConsent() {
        ci.grant(Feature.Lang)
        ci.grant(Feature.Ua)
        ci.grant("test")

        assertEquals(1, ci.campaignConsentSize)
        assertEquals(2, ci.featureConsentSize)

        var consent: Consent? = ci.getFeatureConsentByIndex(0)
        assertNotNull(consent)
        assertTrue(consent!!.granted)

        consent = ci.getFeatureConsentByIndex(1)
        assertNotNull(consent)
        assertTrue(consent!!.granted)

        consent = ci.getFeatureConsentByIndex(2)
        assertNull(consent)

        val midnightTomorrow = Calendar.getInstance()
        midnightTomorrow.set(Calendar.HOUR_OF_DAY, 0)
        midnightTomorrow.set(Calendar.MINUTE, 0)
        midnightTomorrow.set(Calendar.SECOND, 0)
        midnightTomorrow.set(Calendar.MILLISECOND, 0)
        midnightTomorrow.add(Calendar.DAY_OF_MONTH, 1)

        consent = ci.getCampaignConsentByIndex(0)
        assertNotNull(consent)
        assertTrue(consent!!.startDate.after(midnightTomorrow.time))
        assertTrue(consent.granted)

        consent = ci.getCampaignConsentByIndex(1)
        assertNull(consent)

        // Should only ever allowed on the start of the next measurement period,
        // which should be tomorrow, as by the tested configuration.
        assertFalse(ci.isCampaignCurrentlyGranted("test"))
    }

    @Test
    fun persistence() {
        ci.grant(Feature.Lang)
        ci.grant(Feature.Ua)
        ci.grant("test")

        ci.measureVisit(listOf("foo"), "test")
        ci.measureEvent("foo", "bar", "test", "baz", 4567.0)

        ci.persist()

        val store = storedStore

        // Get private store property through reflection.
        var storeField = ci::class.java.getDeclaredField("store")
        storeField.isAccessible = true
        var currentStore = storeField.get(ci) as? DefaultStore

        assertEquals(store.consents, currentStore?.consents)
        assertEquals(listOf<Visit>(), store.visits) // Consent will only start tomorrow!
        assertEquals(listOf<Event>(), store.events) // Consent will only start tomorrow!

        // Re-init with faked store.
        val ci = CleanInsights(confString, fakeYesterdayConsent())

        assertTrue(ci.isCampaignCurrentlyGranted("test"))

        ci.measureVisit(listOf("foo"), "test")
        ci.measureEvent("foo", "bar", "test", "baz", 4567.0)

        ci.persist()

        val midnight = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        midnight.set(Calendar.HOUR_OF_DAY, 0)
        midnight.set(Calendar.MINUTE, 0)
        midnight.set(Calendar.SECOND, 0)
        midnight.set(Calendar.MILLISECOND, 0)

        val midnightTomorrow = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        midnightTomorrow.set(Calendar.HOUR_OF_DAY, 0)
        midnightTomorrow.set(Calendar.MINUTE, 0)
        midnightTomorrow.set(Calendar.SECOND, 0)
        midnightTomorrow.set(Calendar.MILLISECOND, 0)
        midnightTomorrow.add(Calendar.DAY_OF_MONTH, 1)

        // Get private store property through reflection.
        storeField = ci::class.java.getDeclaredField("store")
        storeField.isAccessible = true
        currentStore = storeField.get(ci) as? DefaultStore

        assertEquals(arrayListOf(Visit(listOf("foo"), "test", 1, midnight.toInstant(), midnightTomorrow.toInstant())),
                currentStore?.visits)
        assertEquals(arrayListOf(Event("foo", "bar", "baz", 4567.0, "test", 1, midnight.toInstant(), midnightTomorrow.toInstant())),
                currentStore?.events)
    }

    @Test
    fun purge() {
        val store = TestStore()

        val dayBeforeYesterday = Instant.now().minus(2, ChronoUnit.DAYS)

        store.visits.add(Visit(listOf("foo"), "x", 1, dayBeforeYesterday, dayBeforeYesterday))
        store.events.add(Event("foo", "bar", null, null, "x", 1, dayBeforeYesterday, dayBeforeYesterday))

        // Get private purge method through reflection.
        val purgeMethod = Insights.Companion::class.declaredMemberFunctions.find { it.name == "purge" }
        purgeMethod?.isAccessible = true
        purgeMethod?.call(Insights.Companion, conf, store)

        assertEquals(0, store.visits.size)
        assertEquals(0, store.events.size)

        val now = Instant.now()

        store.visits.add(Visit(listOf("foo"), "x", 1, now, now))
        store.events.add(Event("foo", "bar", null, null, "x", 1, now, now))

        purgeMethod?.call(Insights.Companion, conf, store)

        assertEquals(1, store.visits.size)
        assertEquals(1, store.events.size)
    }

    @Test
    fun serializeInsights() {
        val insights = Insights(1, "foo", "bar")

        assertEquals("{\"idsite\":1,\"lang\":\"foo\",\"ua\":\"bar\"}", CleanInsights.moshi.adapter(Insights::class.java).toJson(insights))
    }

    @Test
    fun serverTest() {
        ci = CleanInsights(conf, TestStore())

        val lock = CountDownLatch(1)

        var error: Exception? = RuntimeException("error not null!")

        ci.testServer {
            error = it
            lock.countDown()
        }

        lock.await(2, TimeUnit.SECONDS)

        assertNull(error)
    }

    @Test
    fun invalidConfigs() {
        testConfigCheck("ftp:", -1, emptyMap(), false)

        testConfigCheck("ftp:", -1, mapOf(Pair("foobar", Campaign(Instant.now(), Instant.now(), 0))), false)

        testConfigCheck("ftp:", 1, mapOf(Pair("foobar", Campaign(Instant.now(), Instant.now(), 0))), false)

        testConfigCheck("https://example.org/", 1, mapOf(Pair("foobar", Campaign(Instant.now(), Instant.now(), 0))), true)
    }

    private val storeFile: File
        get() = File(tempDir.root, "cleaninsights.json")

    private val storedStore: DefaultStore
        get() {
            val json = storeFile.readText()

            val store = CleanInsights.moshi.adapter(DefaultStore::class.java).fromJson(json)

            assertNotNull(store)

            return store!!
        }

    private fun fakeYesterdayConsent(): DefaultStore {
        val store = storedStore

        val yesterday = Calendar.getInstance()
        yesterday.add(Calendar.DAY_OF_MONTH, -1)

        val end = Calendar.getInstance()
        end.add(Calendar.DAY_OF_MONTH, 3)

        store.consents.campaigns["test"] = Consent(granted = true, yesterday.toInstant(), end.toInstant())

        return store
    }

    private fun testConfigCheck(server: String, siteId: Int, campaigns: Map<String, Campaign>, isGood: Boolean) {
        val conf = Configuration(URL(server), siteId, campaigns)

        var count = 0

        val result = conf.check {
            count++
        }

        if (isGood) {
            assertEquals(0, count)
            assertTrue(result)
        }
        else {
            assertEquals(1, count)
            assertFalse(result)
        }
    }

    private class TestStore: Store() {

        override fun load(args: Map<String, Any>): Store? {
            return null
        }

        override fun persist(async: Boolean, done: (e: Exception?) -> Unit) {
            return
        }

        override fun send(data: String, server: URL, timeout: Double, done: (e: Exception?) -> Unit) {
            done(IOException("HTTP Error 400: "))
        }
    }
}