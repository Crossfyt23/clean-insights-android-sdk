/**
 * Consent.kt
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart.
 * Copyright © 2020 Guardian Project. All rights reserved.
 */
@file:Suppress("unused")

package org.cleaninsights.sdk

import com.squareup.moshi.Json
import java.time.Instant
import java.util.*

enum class Feature {
    @Json(name = "lang")
    Lang,

    @Json(name = "ua")
    Ua
}

open class Consent(val granted: Boolean, val start: Instant = Instant.now(), val end: Instant = Instant.now()) {

    val startDate: Date
        get() = Date.from(start)

    val endDate: Date
        get() = Date.from(end)

    constructor(granted: Boolean, period: Period) : this(granted, period.start, period.end)

    override fun equals(other: Any?): Boolean {
        if (other is Consent) {
            return granted == other.granted && start == other.start && end == other.end
        }

        return super.equals(other)
    }

    override fun hashCode(): Int {
        var result = granted.hashCode()
        result = 31 * result + start.hashCode()
        result = 31 * result + end.hashCode()
        return result
    }

    override fun toString(): String {
        return String.format("[%s: granted=%b, start=%s, end=%s]",
                this::class, granted, start, end)
    }
}

class FeatureConsent(val feature: Feature, consent: Consent): Consent(consent.granted, consent.start, consent.end)

class CampaignConsent(val campaignId: String, consent: Consent): Consent(consent.granted, consent.start, consent.end)
